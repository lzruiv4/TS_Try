// 声明一个类的属性
let person: {name: string, age?: number};
person = {name: "lao", age: 20};

console.log(person.name)

//可以有任意属性，就是在中括号中的属性可以多个
let person01: {name:string, [propName: string]: any};
person01 = {name: "lin", age: 90, ks: 999}; //如这里所示，可以有多个属性，但是如果是删除了name，他就会直接报错

// 或者使用函数结构    类型 属性名 （参数1， 参数2 。。。） => 返回值
let person02: (name: string, age: number) => string;

// Array
let personArray: string[];
personArray = ["w", "dd"];

//枚举类
enum Gender{
    Male,
    Female
}

// 对象，有点像java中的class
let personWithGender : {
    name : string,
    gender : Gender,
    age? : number
};

personWithGender = {name : "ooo", gender : Gender.Male};
console.log(personWithGender.gender === Gender.Male);

// 如果必须指定一个属性，其他的属性随便添加，如何表示
let auto : {
    id: number,
    [property: string] : any
}

auto = {
    id: 1, 
    name: 'Lin', 
    age : 30
}

// 类型的别名
type myType = 1 | 2 | 3 | 4 | 5;
let first : myType;
let second : myType;

first = 1;
second = 2;


// never 表示永远不会返回结果,基本上用于抛出错误
function neverTest() : never {
    throw new Error('Wrong!');
}

// 我称他为简化函数的表达 =>  ==== {}
// 设置函数结构的生命类型
let simple : (num1 : number , num2 : number) => number;
simple = function (num1, num2){
    return num1 + num2;
}

function simple1 (num1 : number, num2 : number) : number {
    return num1 + num2;
}

let arrayString : string[];
let arrayString1 : Array<string>;
