// 声明一个类的属性
var person;
person = { name: "lao", age: 20 };
console.log(person.name);
//可以有任意属性，就是在中括号中的属性可以多个
var person01;
person01 = { name: "lin", age: 90, ks: 999 }; //如这里所示，可以有多个属性，但是如果是删除了name，他就会直接报错
// 或者使用函数结构    类型 属性名 （参数1， 参数2 。。。） => 返回值
var person02;
// Array
var personArray;
personArray = ["w", "dd"];
//枚举类
var Gender;
(function (Gender) {
    Gender[Gender["Male"] = 0] = "Male";
    Gender[Gender["Female"] = 1] = "Female";
})(Gender || (Gender = {}));
// 对象，有点像java中的class
var personWithGender;
personWithGender = { name: "ooo", gender: Gender.Male };
console.log(personWithGender.gender === Gender.Male);
// 如果必须指定一个属性，其他的属性随便添加，如何表示
var auto;
auto = {
    id: 1,
    name: 'Lin',
    age: 30
};
var first;
var second;
first = 1;
second = 2;
// never 表示永远不会返回结果,基本上用于抛出错误
function neverTest() {
    throw new Error('Wrong!');
}
// 我称他为简化函数的表达 =>  ==== {}
// 设置函数结构的生命类型
var simple;
simple = function (num1, num2) {
    return num1 + num2;
};
function simple1(num1, num2) {
    return num1 + num2;
}
var arrayString;
var arrayString1;
